package com.example.countriesapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp1.data.Country;

public class DetailFragment extends Fragment {

    Country[] countries = Country.countries;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int countryNum = 0;

        TextView countryNameText = view.findViewById(R.id.itemCountryName);
        ImageView imageView = view.findViewById(R.id.itemImage2);
        EditText capitaleText = view.findViewById(R.id.itemCapitale);
        EditText languageText = view.findViewById(R.id.itemLanguage);
        EditText monnaieText = view.findViewById(R.id.itemMonnaie);
        EditText populationText = view.findViewById(R.id.itemPopulation);
        EditText superficieText = view.findViewById(R.id.itemSuperficie);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        countryNum = args.getCountryId();

        countryNameText.setText(countries[countryNum].getName());
        capitaleText.setText(countries[countryNum].getCapital());
        languageText.setText(countries[countryNum].getLanguage());
        monnaieText.setText(countries[countryNum].getCurrency());
        populationText.setText(Integer.toString(countries[countryNum].getPopulation()));
        superficieText.setText(Integer.toString(countries[countryNum].getArea()));

        String uri = countries[countryNum].getImgUri();
        Context c = imageView.getContext();
        imageView.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri , null , c.getPackageName())));

        view.findViewById(R.id.button_retour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}